package com.chrmills.dynamiccraft.block;

import com.chrmills.dynamiccraft.DynamicCraft;

import net.minecraft.block.BlockFalling;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlackSand extends BlockFalling {
    private static final String REGISTRY_NAME = "black_sand";
    private static final String UNLOCALIZED_NAME = DynamicCraft.MOD_ID + "." + REGISTRY_NAME;

    public BlackSand() {
        super(Material.SAND);
        this.setRegistryName(REGISTRY_NAME);
        this.setUnlocalizedName(UNLOCALIZED_NAME);
        this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
    }

    @SideOnly(Side.CLIENT)
    void initModel() {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0,
            new ModelResourceLocation(this.getRegistryName(), "inventory"));
    }
}
