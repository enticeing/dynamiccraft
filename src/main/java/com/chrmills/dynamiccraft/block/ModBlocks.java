package com.chrmills.dynamiccraft.block;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class ModBlocks {
    @GameRegistry.ObjectHolder("dynamiccraft:black_sand")
    public static BlackSand BLACK_SAND;

    public static void registerBlocks(IForgeRegistry<Block> registry) {
        registry.registerAll(new BlackSand());
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registry.registerAll(
            new ItemBlock(ModBlocks.BLACK_SAND).setRegistryName(BLACK_SAND.getRegistryName()));
    }

    @SideOnly(Side.CLIENT)
    public static void initModels() {
        BLACK_SAND.initModel();
    }
}
