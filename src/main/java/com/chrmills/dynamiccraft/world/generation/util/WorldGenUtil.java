package com.chrmills.dynamiccraft.world.generation.util;

import java.util.Set;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;

public class WorldGenUtil {
    public static BlockPos getSurfaceFromSky(World world, BlockPos skyPos) {
        BlockPos surfacePos = new BlockPos(skyPos);
        while (world.isAirBlock(surfacePos)) {
            surfacePos = surfacePos.down();
        }

        return surfacePos;
    }

    public static boolean isBiomeType(final World world, final BlockPos blockPos, final BiomeDictionary.Type biomeType) {
        Set<Biome> allBiomesOfType = BiomeDictionary.getBiomes(biomeType);
        for (Biome biome : allBiomesOfType) {
            if (world.getBiome(blockPos).getBiomeName().equals(biome.getBiomeName())) {
                return true;
            }
        }

        return false;
    }
}
